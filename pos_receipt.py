# -*- coding: utf-8 -*-
#
# Author:   Dario Necco
# Company:  Perforce Inc.
#
# This script allows to create e pos receipt
import locale
import os
import sys
import argparse
from argparse import ArgumentParser, RawTextHelpFormatter
import logging
import inspect
import time
import shutil
import io
import re
import subprocess
from subprocess import Popen, PIPE
from sys import platform as _platform
import traceback
import pprint
from distutils import dir_util
from xml.dom import minidom
import datetime
import jinja2
import imgkit
# import chardet  # uncomment only if necessary, it requires installation

# import Namespace
try:
    from types import SimpleNamespace as Namespace  # available from Python 3.3
except ImportError:
    class Namespace:
        def __init__(self, **kwargs):
            self.__dict__.update(kwargs)

if sys.version[0] == '2':
    from imp import reload

    reload(sys)
    sys.setdefaultencoding("utf-8")

# import Colorama
try:
    from colorama import init, Fore, Back, Style
    init(strip=True)  # strip makes colorama working in PyCharm
except ImportError:
    print('Colorama not imported')

locale.setlocale(locale.LC_ALL, 'C')  # set locale

# set version and author  
__version__ = 1.0

# I obtain the app directory
if getattr(sys, 'frozen', False):
    # frozen
    dirapp = os.path.dirname(sys.executable)
    dirapp_bundle = sys._MEIPASS
    executable_name = os.path.basename(sys.executable)
else:
    # unfrozen
    dirapp = os.path.dirname(os.path.realpath(__file__))
    dirapp_bundle = dirapp
    executable_name = os.path.basename(__file__)

##############################################################################################
# DEBUG
this_scriptFile = inspect.getfile(inspect.currentframe())
this_scriptFile_filename = os.path.basename(this_scriptFile)
this_scriptFile_filename_noext, ext = os.path.splitext(this_scriptFile_filename)

# logging.basicConfig(filename=this_scriptFile_filename_noext + '.log', level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')  # uncomment for Logging

print('Working dir: \"' + dirapp + '\"')
welcome_text = '{char1} {appname} v.{version} {char2}'.format(char1='-' * 5, 
                                                              appname=os.path.splitext(os.path.basename(__file__))[0], 
                                                              version=__version__, char2='-' * 5)
print(welcome_text)
logging.info(welcome_text)


def print_color(text, color):
    print('{color}{text}{reset_color}'.format(color=color, text=text, reset_color=Style.RESET_ALL))


def print_error(text):
    print('{color}{text}{reset_color}'.format(color=Fore.RED, text=text, reset_color=Style.RESET_ALL))


def print_warning(text):
    print('{color}{text}{reset_color}'.format(color=Fore.YELLOW, text=text, reset_color=Style.RESET_ALL))


def check_args():
    parser = ArgumentParser(formatter_class=RawTextHelpFormatter, description="""
    Description

    """)

    # Options
    parser.add_argument("-debug", dest="debug", action='store_true', help=argparse.SUPPRESS)

    parser.add_argument("-c", "--currency", dest="currency", choices=('$', '£', '€'), default='£', help="Currency to use in the receipt")
                        # help='Increase output verbosity. Output files don\'t overwrite original ones'

    #parser.add_argument("-op", "--output", dest="output_path",
                        #help="qac output path. it has given in input to this application")
                        # action='store_true'
                        # nargs="+",
                        # nargs='?',  # optional argument
                        # default=""
                        # type=int
                        # choices=[]

    #parser.add_argument("source_file", help="source file must be analyzed")

    args = parser.parse_args()  # it returns input as variables (args.dest)

    # end check args

    return args


def format_amount(amount, country):
    amount = float(amount)
    amount_formatted = '{:20,.2f}'.format(amount).strip()

    if country.upper() == 'IT':
        amount_formatted = amount_formatted.replace('.', '<dot>').replace(',', '<comma>')
        amount_formatted = amount_formatted.replace('<dot>', ',').replace('<comma>', '.')

    return amount_formatted


def round_str(number, useless_dec=True, r=2):
    """
    rounds the number making it integer if decimal are useless
    """
    rn = format(number, '.{}f'.format(r))  # => float

    if str(rn).endswith('.' + '0'*r):
        if useless_dec:
            pass
        else:
            rn = str(int(float(rn)))
    else:
        pass

    return rn


def run_cmd(cmd, cwd=os.path.dirname(os.path.realpath(__file__))):
    p = Popen(cmd, cwd=cwd, stdout=PIPE, stderr=PIPE, shell=True)
    p_out, p_err = p.communicate()
    p_rc = p.returncode

    p_out.decode('utf-8')


def html_to_img(inputhtml, outimg=False, zoom=False):
    if not outimg:
        outimg = os.path.splitext(os.path.realpath(inputhtml))[0] + '.jpg'

    if not zoom:
        imgkit.from_file(inputhtml, outimg)
    else:
        cmd = 'wkhtmltoimage --zoom {} "{}" "{}"'.format(zoom, inputhtml, outimg)
        print(cmd)
        run_cmd(cmd)


def main(args=None):
    if args is None:
        args = check_args()

    # temp inputs
    items = [
        ['Avo Bacon', '1', '20']
    ]

    total = sum([float  (x[2]) for x in items])

    templates_dir = os.path.join(os.path.realpath(os.path.dirname(__file__)), 'template')
    template_env = jinja2.Environment(loader=jinja2.FileSystemLoader(templates_dir, encoding='utf-8', followlinks=False), trim_blocks=True, lstrip_blocks=True, autoescape=True)  
    template_env.filters['round_str'] = round_str  # add custom filter
    loaded_template = template_env.get_template("pos-receipt.html")  # template file to render  
    result = loaded_template.render(currency=args.currency, items=items, total=round_str(total, useless_dec=True, r=2))  # list of variable=value to pass to the template file to call

    outhtmlfile = 'receipt.html'
    outhtmlfile = os.path.abspath(outhtmlfile)
    print(outhtmlfile)
    with open(outhtmlfile, 'w', encoding='utf-8', errors='ignore') as w_out:
        w_out.write(result)

    html_to_img(outhtmlfile, zoom=3)


if __name__ == '__main__':
    try:
        main(args=None)
    except KeyboardInterrupt:
        print('\n\nBye!')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)

# DONE : calculate total
# TODO : export to image
# TODO : zoom in export
